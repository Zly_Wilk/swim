package com.example.zadanie1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Button przycisk = (Button) findViewById(R.id.button5);

        przycisk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Toast.makeText(getApplicationContext(), "Wróć", Toast.LENGTH_SHORT).show();
            }
        });

        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
        final TextView tekst = (TextView) findViewById(R.id.textView);


        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    tekst.setText("Włączony");
                    Toast.makeText(getApplicationContext(), "Włączony", Toast.LENGTH_SHORT).show();
                }else{
                    tekst.setText("Wyłączony");
                    Toast.makeText(getApplicationContext(), "Wyłączony", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        final TextView tekst2 = (TextView) findViewById(R.id.textView2);

        switch (view.getId()){
            case R.id.radioButton:
                if(checked){
                    tekst2.setText("Tak");
                    Toast.makeText(getApplicationContext(), "Tak", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.radioButton2:
                if(checked){
                    tekst2.setText("Nie");
                    Toast.makeText(getApplicationContext(), "Nie", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.radioButton3:
                if(checked){
                    tekst2.setText("Nie wiem");
                    Toast.makeText(getApplicationContext(), "Nie wiem", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
