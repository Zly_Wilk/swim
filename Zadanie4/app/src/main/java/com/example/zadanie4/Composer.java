package com.example.zadanie4;

public class Composer {
    private int nickname;
    private String name;
    private String surname;

    private int genre;
    private int adoreRating;

    public Composer(String name, String surname, int nickname,  int genre, int adoreRating)
    {
        this.nickname=nickname;
        this.name=name;
        this.surname=surname;

        this.genre =genre;
        this.adoreRating =adoreRating;
    }

    public int getNickname() {
        return nickname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNickname(int nickname) {
        this.nickname = nickname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getGenre() {
        return genre;
    }

    public void setGenre(int genre) {
        this.genre = genre;
    }

    public int getAdoreRating() {
        return adoreRating;
    }

    public void setAdoreRating(int adoreRating) {
        this.adoreRating = adoreRating;
    }


    @Override
    public String toString() {

        return getName();
    }
}
