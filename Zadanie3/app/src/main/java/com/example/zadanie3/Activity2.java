package com.example.zadanie3;

import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView headText;
    TextView lyricText;

    private boolean mBold = false;
    private boolean mItalic = false;

    private ActionMode actionMode;

    ConstraintLayout c2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        headText= (TextView) findViewById(R.id.textView3);
        lyricText= (TextView) findViewById(R.id.textView4);

        c2= findViewById(R.id.layout2);

        Button przycisk0 = (Button) findViewById(R.id.button30);
        Button przycisk1 = (Button) findViewById(R.id.button31);
        Button przycisk2 = (Button) findViewById(R.id.button32);


        registerForContextMenu(przycisk0);
        registerForContextMenu(przycisk2);

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actionMode == null)
                    actionMode = startActionMode(actionModeCallback);

            }
        });
    }

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            mode.getMenuInflater().inflate(R.menu.menu2, menu);
            mode.setTitle("Action Mode");

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {


            switch ((item.getItemId())) {

                case R.id.itemB: {
                    if (mBold==true) {
                        mBold=false;
                    }else {
                        mBold=true;
                    }
                }
                break;
                case R.id.itemI: {
                    if (mItalic==true) {
                        mItalic=false;
                    }else {
                        mItalic=true;
                    }
                }
                break;

            }
            updateTextView();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0,0,1, "Czarny");
        menu.add(0,1,2, "Czerwony");
        menu.add(0,2,3, "Niebieski");
        SubMenu subMenu = menu.addSubMenu(0,3,4, "Rozmiar");
        subMenu.add(0,4,1, "14");
        subMenu.add(0,5,2, "18");
        subMenu.add(0,6,3, "24");


        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case 0:
                headText.setTextColor(getResources().getColor(R.color.czarny));
                lyricText.setTextColor(getResources().getColor(R.color.czarny));
                break;
            case 1:
                headText.setTextColor(getResources().getColor(R.color.czerwony));
                lyricText.setTextColor(getResources().getColor(R.color.czerwony));
                break;
            case 2:
                headText.setTextColor(getResources().getColor(R.color.niebieski));
                lyricText.setTextColor(getResources().getColor(R.color.niebieski));
                break;
            case 4:
                headText.setTextSize(14);
                lyricText.setTextSize(14);
                break;
            case 5:
                headText.setTextSize(18);
                lyricText.setTextSize(18);
                break;
            case 6:
                headText.setTextSize(24);
                lyricText.setTextSize(24);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        switch (v.getId())
        {
            case R.id.button30:
            {
                menu.add(0, 0, 1, "Nagłówek czarny");
                menu.add(0, 1, 2, "Nagłówek czerwony");
                menu.add(0, 2, 3, "Nagłówek niebieski");
                menu.add(0, 3, 4, "Wielkość tekstu 14");
                menu.add(0, 4, 5, "Wielkość tekstu 18");
                menu.add(0, 5, 6, "Wielkość tekstu 24");

            }
            break;
            case R.id.button32:
            {
                menu.setHeaderTitle("Kolor tła");
                menu.add(0,6,1,"Biały");
                menu.add(0,7,2,"Niebieski");
                menu.add(0,8,3,"Zielony");
                menu.setGroupCheckable(0,true,true);


            }
            break;
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case(0):
            {
                headText.setTextColor(getResources().getColor(R.color.czarny));
                return true;
            }
            case(1):
            {
                headText.setTextColor(getResources().getColor(R.color.czerwony));
                return true;
            }
            case(2):
            {
                headText.setTextColor(getResources().getColor(R.color.niebieski));
                return true;
            }
            case(3):
            {
                lyricText.setTextSize(14);
                return true;
            }
            case(4):
            {
                lyricText.setTextSize(18);
                return true;
            }
            case(5):
            {
                lyricText.setTextSize(24);
                return true;
            }
            case(6):
            {
                item.setChecked(true);
                c2.setBackgroundColor(getResources().getColor(R.color.bialy));

                return true;
            }
            case(7):
            {
                item.setChecked(true);
                c2.setBackgroundColor(getResources().getColor(R.color.niebieski));
                return true;
            }
            case(8):
            {
                item.setChecked(true);
                c2.setBackgroundColor(getResources().getColor(R.color.zielony));
                return true;
            }


            default:
                return super.onContextItemSelected(item);
        }

    }

    protected void updateTextView(){
        if(mBold && mItalic){
            headText.setTypeface(null, Typeface.BOLD_ITALIC);
            lyricText.setTypeface(null, Typeface.BOLD_ITALIC);
        }else if(mBold && !mItalic){
            headText.setTypeface(null,Typeface.BOLD);
            lyricText.setTypeface(null,Typeface.BOLD);
        }else if(!mBold && mItalic){
            headText.setTypeface(null,Typeface.ITALIC);
            lyricText.setTypeface(null,Typeface.ITALIC);
        }else {
            headText.setTypeface(null,Typeface.NORMAL);
            lyricText.setTypeface(null,Typeface.NORMAL);
        }
    }
}
