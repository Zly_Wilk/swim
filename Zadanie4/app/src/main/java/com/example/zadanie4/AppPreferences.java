package com.example.zadanie4;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import java.lang.reflect.Type;
import java.util.ArrayList;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;



public class AppPreferences {
    private SharedPreferences _sharedPrefs;
    private SharedPreferences.Editor _prefsEditor;
    private Gson gson;
    public AppPreferences(Context context)
    {
        this._sharedPrefs = context.getSharedPreferences("apk_data", Activity.MODE_PRIVATE);
        this._prefsEditor = _sharedPrefs.edit();
        this.gson = new Gson();
    }

    public void saveComposerArray(ArrayList<Composer> composerArray)
    {
        String jsonComposerArray = gson.toJson(composerArray);
        _prefsEditor.putString("composerArray", jsonComposerArray);
        _prefsEditor.commit();
    }
    public ArrayList<Composer> getComposerArray()
    {
        String jsonComposerArray = _sharedPrefs.getString("composerArray",null);
        if (jsonComposerArray==null) return new ArrayList<Composer>();
        Type typeComposerList = new TypeToken<ArrayList<Composer>>(){}.getType();
        ArrayList<Composer> composerArray = gson.fromJson(jsonComposerArray,typeComposerList);
        return composerArray;
    }
    public void resetComposerArray()
    {
        String jsonComposerArray = gson.toJson(new ArrayList<Composer>());
        _prefsEditor.putString("composerArray", jsonComposerArray);
        _prefsEditor.commit();
    }
    public void saveSongArray(ArrayList<Song> songArray)
    {
        String jsonSongArray = gson.toJson(songArray);
        _prefsEditor.putString("songArray", jsonSongArray);
        _prefsEditor.commit();
    }
    public ArrayList<Song> getSongArray()
    {
        String jsonSongArray = _sharedPrefs.getString("songArray",null);
        if (jsonSongArray==null) return new ArrayList<Song>();
        Type typeSongList = new TypeToken<ArrayList<Song>>(){}.getType();
        ArrayList<Song> songArray = gson.fromJson(jsonSongArray,typeSongList);
        return songArray;
    }

    public void resetSongArray()
    {
        String jsonComposerArray = gson.toJson(new ArrayList<Song>());
        _prefsEditor.putString("songArray", jsonComposerArray);
        _prefsEditor.commit();
    }

}
