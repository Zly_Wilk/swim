package com.example.cw1;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main2Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        View mojeOkno2 = (View) findViewById(R.id.mojeOkno2);
        mojeOkno2.setOnLongClickListener(new View.OnLongClickListener(){
            public boolean onLongClick(View v){
                finish();
                return false;
            }
        });
    }
}
