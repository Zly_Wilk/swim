package com.example.zadanie4;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;



public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, BottomNavigationView.OnNavigationItemSelectedListener, ChoiceFragment.OnWyborOpcjiListener{


    public static final int _ADDING = 0, _COMPOSERS_LIST = 1, _SONGS_LIST = 2;
    private ViewPager mViewPager;
    private TextView mTextMessage;
    static int opcjaa;
    private BottomNavigationView mBottomNavigationView;
    AppPreferences _appPrefs;
    AddingCompFragment addCompFrag;
    AddingSongFragment addSongFrag;
    ComposersListFragment listCompFrag;
    FragmentTransaction transakcja;
    SectionsPagerAdapter sectionAdapter;
    private static final String TAG_addComp = "AddingCompFragment";
    private static final String TAG_addSong = "AddingSongFragment";
    SongsListFragment songsListFragment;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _appPrefs = new AppPreferences(getApplicationContext());
        mViewPager = findViewById(R.id.container);

        mViewPager.setOffscreenPageLimit(2);
        FragmentManager fragmentManager=getSupportFragmentManager();
        sectionAdapter = new SectionsPagerAdapter(fragmentManager);

        mViewPager.setAdapter(sectionAdapter);
        mViewPager.addOnPageChangeListener(this);
        mTextMessage = findViewById(R.id.message);
        mBottomNavigationView =  findViewById(R.id.navigation);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);






        if(savedInstanceState == null)
        {

            addCompFrag = new AddingCompFragment();
            addSongFrag = new AddingSongFragment();

            transakcja = getSupportFragmentManager().beginTransaction();
            transakcja.add(R.id.kontener, addCompFrag, TAG_addComp);
            transakcja.detach(addCompFrag);
            transakcja.add(R.id.kontener, addSongFrag, TAG_addSong);
            transakcja.detach(addSongFrag);
            transakcja.commit();
        }
        else
        {

            addCompFrag = (AddingCompFragment) getSupportFragmentManager().findFragmentByTag(TAG_addComp);
            addSongFrag = (AddingSongFragment) getSupportFragmentManager().findFragmentByTag(TAG_addSong);

        }


       /* listCompFrag =(ComposersListFragment) sectionAdapter.getItem(_COMPOSERS_LIST);
        listCompFrag.composerListView.setOnItemClickListener( new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<Composer> composerArray = _appPrefs.getComposerArray();
                Composer composer = composerArray.get(position);
                Intent intent = new Intent(view.getContext(), ComposerInfo.class);

                Toast.makeText(view.getContext(),
                        "Click",
                        Toast.LENGTH_SHORT).show();

                intent.putExtra("textInfoName",composer.getName());
                intent.putExtra("textInfoSurname",composer.getSurname());
                intent.putExtra("textInfoNickname",composer.getNickname());
                intent.putExtra("textInfoGenre",composer.getGenre());
                intent.putExtra("textInfoAdore",composer.getAdoreRating());
                startActivity(intent);
            }

        });*/
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addButton:
                mViewPager.setCurrentItem(_ADDING);
                changeTitle(_ADDING);
                return true;
            case R.id.composerListButton:
                mViewPager.setCurrentItem(_COMPOSERS_LIST);
                changeTitle(_COMPOSERS_LIST);
                return true;
            case R.id.songsListButton:

                mViewPager.setCurrentItem(_SONGS_LIST);
                changeTitle(_SONGS_LIST);
                return true;
        }
        return false;
    }










    public void changeTitle(int position) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;

        if (position == _ADDING)
            actionBar.setTitle("Dodaj");
        else if (position == _COMPOSERS_LIST)
            actionBar.setTitle("Albumy");
        else if (position == _SONGS_LIST)
            actionBar.setTitle("Utwory");
    }

    @Override
    public void onWyobrOpcji(int opcja) {
        FragmentTransaction transakcja = getSupportFragmentManager().beginTransaction();
        switch (opcja) {

            case 1:
                transakcja.detach(addSongFrag);
                transakcja.attach(addCompFrag);
                transakcja.commit();
                break;
            case 2:
                transakcja.detach(addCompFrag);
                transakcja.attach(addSongFrag);
                transakcja.commit();
                break;
        }

    }



    public void addComposer(View view)
    {
        addCompFrag.addComposer(view);
        listCompFrag =(ComposersListFragment) sectionAdapter.getItem(_COMPOSERS_LIST);
        listCompFrag.listRefresh();







    }


    public void addSong(View view)
    {

        addSongFrag.addSong(view);
        songsListFragment =(SongsListFragment) sectionAdapter.getItem(_SONGS_LIST);
        songsListFragment.listRefresh();

    }
    public void resetElements(View view)
    {
        addCompFrag.resetElements(view);
        _appPrefs.resetComposerArray();


    }
    public void resetElementsSong(View view)
    {
        addSongFrag.resetElementsSong(view);
        _appPrefs.resetSongArray();
    }


    @Override
    public void onPageScrolled(int i, float v, int i1) {



    }

    @Override
    public void onPageSelected(int position) {


        //mViewPager.getAdapter().notifyDataSetChanged();



        mBottomNavigationView.getMenu().getItem(position).setChecked(true);
        changeTitle(position);
    }

    @Override
    public void onPageScrollStateChanged(int i) {


    }

}
