package com.example.zadanie4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ComposerInfo extends AppCompatActivity {

    String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_composer_info);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Informacje o albumie");

        TextView name =  findViewById(R.id.textInfoName);
        TextView surname = findViewById(R.id.textInfoSurname);
        TextView nick = findViewById(R.id.textInfoNickname);
        TextView genre = findViewById(R.id.textInfoGenre);
        TextView adore = findViewById(R.id.textInfoAdore);


        /*
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null)
            {




                name.setText(extras.getString("textInfoName"));
                surname.setText(extras.getString("textInfoSurname"));
                nick.setText(extras.getString("textInfoNickname"));
                genre.setText(musicalGenres[extras.getString("textInfoGenre").toCharArray()[1]-'0']);
                adore.setText("");

            }
        } else {
            name.setText((String) savedInstanceState.getSerializable("textInfoName"));
            surname.setText((String) savedInstanceState.getSerializable("textInfoSurname"));
            nick.setText((String) savedInstanceState.getSerializable("textInfoNickname"));
            genre.setText(musicalGenres[(int)savedInstanceState.getSerializable("textInfoGenre")-1]);
            adore.setText("");

        }*/

        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);

        Intent iii= getIntent();
        Bundle dane= new Bundle();
        dane= iii.getExtras();
        int nr= dane.getInt("numer");

        String klucz2= "album"+nr;
        name.setText(sp.getString(klucz2, "DEFAULT"));
        String klucz3= "wykonawca"+nr;
        surname.setText(sp.getString(klucz3, "DEFAULT"));
        String klucz4= "rok"+nr;
        nick.setText(sp.getString(klucz4, "DEFAULT"));
        String klucz5= "gatunek0"+nr;
        genre.setText(musicalGenres[sp.getInt(klucz5, 0)]);
        adore.setText("");
    }

}
