package com.example.zadanie2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Settings extends AppCompatActivity {

    String[] lista= {"Witam", "Dzień dobry", "Cześć"};

    int BGC;
    int tempI=1;
    String tempS="";
    String tekstNaglowka;
    String tekstPowitalny;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
        final SharedPreferences.Editor spe=sp.edit();
        tekstPowitalny=sp.getString("tekstP", "Witam");
        tekstNaglowka=sp.getString("tekstN", "Zadanie 2");
        BGC=sp.getInt("kolor", 255);
        spe.putString("tekstP", tekstPowitalny);
        spe.putString("tekstN", tekstNaglowka);
        spe.putInt("kolor", BGC);
        spe.commit();



        final ConstraintLayout cl= findViewById(R.id.settingsLayout);
        cl.setBackgroundColor(Color.rgb(BGC, BGC, BGC));

        Button przycisk4 = (Button) findViewById(R.id.button4);
        Button przycisk5 = (Button) findViewById(R.id.button5);
        Button przycisk6 = (Button) findViewById(R.id.button6);

        przycisk4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BGC=255;
                tempI=255;
                tempS="Zadanie 2";
                tekstPowitalny="Witam";
                tekstNaglowka= "Zadanie 2";
                cl.setBackgroundColor(Color.rgb(BGC, BGC, BGC));

            }
        });
        przycisk5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tempI!=1) BGC= tempI;
                if(tempS!="")tekstNaglowka= tempS;
                spe.putString("tekstN", tekstNaglowka);
                spe.putString("tekstP", tekstPowitalny);
                spe.putInt("kolor", BGC);
                spe.commit();
            }
        });
        przycisk6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle dane= new Bundle();
                Intent iii= new Intent();
                dane.putInt("kolorTla", BGC);
                dane.putString("naglowek", tekstNaglowka);
                iii.putExtras(dane);
                setResult(Activity.RESULT_OK, iii);
                finish();
            }
        });

        Spinner opcje= (Spinner) findViewById(R.id.spinner);
        if (opcje!=null){
            opcje.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    tekstPowitalny=lista[position];
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            ArrayAdapter<String> adaper= new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, lista);
            adaper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            opcje.setAdapter(adaper);
        }


        //final LinearLayout ll= findViewById(R.id.listLayout);
        //final ConstraintLayout cl2= findViewById(R.id.addLayout);
        //final LinearLayout ll2= findViewById(R.id.mainLayout);

        SeekBar suwak = (SeekBar) findViewById(R.id.seekBar);
        suwak.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                tempI = -progress;

                cl.setBackgroundColor(Color.rgb(tempI, tempI, tempI));

                //ll.setBackgroundColor(Color.rgb(progress, progress, progress));
                //cl2.setBackgroundColor(Color.rgb(progress, progress, progress));
                //ll2.setBackgroundColor(Color.rgb(progress, progress, progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        /*
        RelativeLayout currentLayout =
                    (RelativeLayout) findViewById(R.id.foo);

        currentLayout.setBackgroundColor(Color.RED);

        linearLayout.setBackgroundColor(Color.rgb(0xf4,0x43,0x36))
         */
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()){
            case R.id.radioButton:
                if(checked){
                    tempS= "Zadanie 2";

                }
                break;
            case R.id.radioButton2:
                if(checked){
                    tempS= "Kacper Wilczkiewicz";

                }
                break;
            case R.id.radioButton3:
                if(checked){
                    tempS= "Zadanie 2/ Kacper Wilczkiewicz";

                }
                break;
        }
    }

}
