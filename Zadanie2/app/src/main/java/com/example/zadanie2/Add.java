package com.example.zadanie2;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Toast;

public class Add extends AppCompatActivity {

    String album="";
    String wykonawca="";
    int rok=-1;
    float rating=-1;
    int gatunek=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        final ConstraintLayout ll= findViewById(R.id.addLayout);
        final SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
        int BGC = sp.getInt("kolor", 255);
        ll.setBackgroundColor(Color.rgb(BGC, BGC, BGC));

        Button przycisk7 = (Button) findViewById(R.id.button7);
        Button przycisk8 = (Button) findViewById(R.id.button8);

        final EditText et1= (EditText) findViewById(R.id.editText1);
        final EditText et2= (EditText) findViewById(R.id.editText2);
        final EditText et3= (EditText) findViewById(R.id.editText3);

        final RatingBar rt= (RatingBar) findViewById(R.id.ratingBar);


        przycisk7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                album = et1.getText().toString();
                wykonawca = et2.getText().toString();
                rok = Integer.parseInt(et3.getText().toString());
                rating = rt.getRating();

                if(album==""||wykonawca==""||rok==-1||rating==-1||gatunek==-1){
                    Toast.makeText(getApplicationContext(), "Musisz wybrać wszystkie wartości.", Toast.LENGTH_SHORT).show();
                }else {
                    SharedPreferences.Editor spe = sp.edit();



                    int temp = sp.getInt("num", -1);
                    temp++;

                    spe.putString("album"+temp, album);
                    spe.putString("wykonawca"+temp, wykonawca);
                    spe.putInt("rok"+temp, rok);
                    spe.putFloat("rating"+temp, rating);
                    spe.putInt("gatunek"+temp, gatunek);
                    spe.putInt("num", temp);
                    spe.commit();

                }
            }
        });
        przycisk8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()){
            case R.id.radioButton4:
                if(checked){
                    gatunek=0;//rock

                }
                break;
            case R.id.radioButton5:
                if(checked){
                    gatunek=1;//metal

                }
                break;
            case R.id.radioButton6:
                if(checked){
                    gatunek=2;//jazz

                }
                break;
        }
    }
}
