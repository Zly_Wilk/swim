package com.example.zadanie6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button przycisk0 = findViewById(R.id.button00);
        Button przycisk1 = findViewById(R.id.button01);

        final Intent intencja0 = new Intent(this, WriteNote.class);
        final Intent intencja1 = new Intent(this, Readnote.class);

        przycisk0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja0);

            }
        });

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja1);

            }
        });
    }
}
