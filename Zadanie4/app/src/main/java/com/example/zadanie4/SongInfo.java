package com.example.zadanie4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SongInfo extends AppCompatActivity {

    String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_info);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Informacje o utowrze");

        TextView name  = findViewById(R.id.textInfoName);
        TextView time  = findViewById(R.id.textInfoTime);
        TextView composer = findViewById(R.id.textInfoComposer);
        TextView adore = findViewById(R.id.textInfoAdore);

        /*
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null)
            {




                name.setText(extras.getString("textInfoName"));
                time.setText(extras.getString("textInfoTime"));
                composer.setText(musicalGenres[extras.getInt("textInfoGenre")]);
                adore.setText("");

            }
        } else {
            name.setText((String) savedInstanceState.getSerializable("textInfoName"));


            adore.setText("");

        }*/


        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);

        Intent iii= getIntent();
        Bundle dane= new Bundle();
        dane= iii.getExtras();
        int nr= dane.getInt("numer");

        String klucz2= "tytul"+nr;
        name.setText(sp.getString(klucz2, "DEFAULT"));
        String klucz3= "dlugosc"+nr;
        time.setText(sp.getString(klucz3, "DEFAULT"));
        String klucz5= "gatunek1"+nr;
        composer.setText(musicalGenres[sp.getInt(klucz5, 0)]);
        adore.setText("");
    }
}
