package com.example.zadanie4;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;




import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComposersListFragment extends Fragment  {
    String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};

    //public CustomArrayAdapter adapter;
    public MyAdapter adapter;
    public ArrayList<Composer> composerArray;
    public ListView composerListView;
    View v;
    AppCompatActivity testA;
    public static ComposersListFragment newInstance() {
        return new ComposersListFragment();
    }
    //SharedPreferences sp;
    Context thisContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        thisContext = context;
    }

    AppPreferences _appPrefs;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_composers_list, container, false);
        _appPrefs = new AppPreferences(v.getContext());
        composerListView = v.findViewById(R.id.composerListView);
        composerArray = new ArrayList<>();
        composerArray = _appPrefs.getComposerArray();


        //adapter = new CustomArrayAdapter(v.getContext(), composerArray);
        adapter = new MyAdapter(new String[0]);
        //ListView lista = (ListView) findViewById(R.id.listView);
        //lista.setAdapter(adapter);
        composerListView.setAdapter(adapter);
        /*
        composerListView.setOnItemClickListener( new OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    //ArrayList<Composer> composerArray = _appPrefs.getComposerArray();
                    //if(composerArray.size()==0){
                    //    return;
                    //}
                    Composer composer = composerArray.get(position);
                    //Composer composer = new Composer();

                    Intent intent = new Intent(view.getContext(), ComposerInfo.class);


                    intent.putExtra("textInfoName", " " + composer.getName());
                    intent.putExtra("textInfoSurname", " " + composer.getSurname());
                    intent.putExtra("textInfoNickname", " " + composer.getNickname());
                    intent.putExtra("textInfoGenre", " " + composer.getGenre());
                    intent.putExtra("textInfoAdore", " " + composer.getAdoreRating());
                    startActivity(intent);
                }catch (Exception ex){
                    ex.printStackTrace();

                }
            }

        });*/
        return v;
    }





    public void listRefresh() {
        composerArray = _appPrefs.getComposerArray();
        //adapter = new CustomArrayAdapter(getContext(), composerArray);
        adapter = new MyAdapter(new String[0]);
        composerListView.setAdapter(adapter);

    }

    int liczbaElem;

    private class LVitem{

        TextView tv1;
        TextView tv2;

        Button btt;

    }

    public class MyAdapter extends BaseAdapter {


        private LayoutInflater inflater= null;
        boolean[] zazn_pozycje;
        LVitem myLVitem;

        MyAdapter(String[] lista){
            super();

            SharedPreferences sp= thisContext.getSharedPreferences("dane_plikacji", MODE_PRIVATE);

            zazn_pozycje=new boolean[sp.getInt("num0", 0)];
            inflater= (LayoutInflater) thisContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount(){
            SharedPreferences sp= thisContext.getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            return sp.getInt("num0", 0)+1;
        }

        public Object getItem(int position){
            return position;
        }

        public long getItemId(int position){
            return position;
        }

        public View getView(final int position, View listItemView, ViewGroup parent){
            if(listItemView==null){
                listItemView=inflater.inflate(R.layout.list_row, null);
                myLVitem=new LVitem();
                myLVitem.tv1=(TextView) listItemView.findViewById(R.id.row_tv1);
                myLVitem.tv2=(TextView) listItemView.findViewById(R.id.textGrade);
                myLVitem.btt= (Button) listItemView.findViewById(R.id.button2);

                listItemView.setTag(myLVitem);
            }else {
                myLVitem=(LVitem)listItemView.getTag();
            }

            final SharedPreferences sp= thisContext.getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            liczbaElem=sp.getInt("num0", 0);

            String klucz= "album"+position;
            String klucz2= "gatunek0"+position;
            myLVitem.tv1.setText(sp.getString(klucz, "DEFAULT"));
            myLVitem.tv2.setText(musicalGenres[sp.getInt(klucz2, 0)]);


            myLVitem.btt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pleaseMakeThisWork(position);
                }
            });


            return listItemView;
        }
    }


    void pleaseMakeThisWork(int n){
        Intent intencja = new Intent(thisContext, ComposerInfo.class);
        Bundle dane= new Bundle();
        dane.putInt("numer", n);
        intencja.putExtras(dane);
        startActivityForResult(intencja,2);
    }

    /*
    @Override
    protected void onActivityResult(int reqID, int resC, Intent ii){
        if(resC== Activity.RESULT_OK && reqID==2){

        }
    }*/
}
