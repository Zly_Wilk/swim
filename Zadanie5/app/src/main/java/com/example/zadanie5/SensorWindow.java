package com.example.zadanie5;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SensorWindow extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSrMgr = null;
    private int mSensorType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        mSrMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorType = getIntent().getIntExtra("sensorType", Sensor.TYPE_LIGHT);


        switch (mSensorType){
            case Sensor.TYPE_LIGHT:
                setTitle("Light");
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                setTitle("Acceleration");
                break;
            case Sensor.TYPE_GRAVITY:
                setTitle("Gravity");
                break;
            case Sensor.TYPE_PROXIMITY:
                setTitle("Proximity");
                break;
            case Sensor.TYPE_GYROSCOPE:
                setTitle("Giroscope");
                break;
            case Sensor.TYPE_ORIENTATION:
                setTitle("Orientation");
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        final Sensor sens = mSrMgr.getSensorList(mSensorType).get(0);
        mSrMgr.registerListener(this, sens, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();

        mSrMgr.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tv = (TextView) findViewById(R.id.txt_data);
        StringBuilder sb = new StringBuilder();

        switch (mSensorType){
            case Sensor.TYPE_LIGHT:
                sb.append("Ambient light level: ");
                sb.append(event.values[0]);
                sb.append(" lux");
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                sb.append("X acceleration: ");
                sb.append(String.format("%7.4f", event.values[0]));
                sb.append(" m/s\u00B2\nY acceleration: ");
                sb.append(String.format("%7.4f", event.values[1]));
                sb.append(" m/s\u00B2\nZ acceleration: ");
                sb.append(String.format("%7.4f", event.values[2]));
                sb.append(" m/s\u00B2");
                break;
            case Sensor.TYPE_GRAVITY:
                sb.append("X gravity: ");
                sb.append(String.format("%7.4f", event.values[0]));
                sb.append(" m/s\u00B2\nY gravity: ");
                sb.append(String.format("%7.4f", event.values[1]));
                sb.append(" m/s\u00B2\nZ gravity: ");
                sb.append(String.format("%7.4f", event.values[2]));
                sb.append(" m/s\u00B2");
                break;
            case Sensor.TYPE_PROXIMITY:
                sb.append("Distance: ");
                if(event.values[0]==0)  sb.append("near");
                else sb.append("far");
                break;
            case Sensor.TYPE_GYROSCOPE:
                sb.append("X rotation: ");
                sb.append(String.format("%7.4f", event.values[0]));
                sb.append(" rad/s\nY rotation: ");
                sb.append(String.format("%7.4f", event.values[1]));
                sb.append(" rad/s\nZ rotation: ");
                sb.append(String.format("%7.4f", event.values[2]));
                sb.append(" rad/s");
                break;
            case Sensor.TYPE_ORIENTATION:
                sb.append("X rotation: ");
                sb.append(String.format("%7.4f", event.values[0]));
                sb.append(" degrees\nY rotation: ");
                sb.append(String.format("%7.4f", event.values[1]));
                sb.append(" degrees\nZ rotation: ");
                sb.append(String.format("%7.4f", event.values[2]));
                sb.append(" degrees");
                break;
            default:
                break;
        }

        tv.setText(sb);

        tv = (TextView) findViewById(R.id.txt_status);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("\nAccuracy: ");
        sb2.append(event.accuracy == 3 ? "High" :
                (event.accuracy == 2 ? "Medium" : "Low"));
        tv.setText(sb2);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
