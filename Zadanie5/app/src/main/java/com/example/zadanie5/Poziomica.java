package com.example.zadanie5;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Poziomica extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSrMgr = null;
    private int mSensorType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poziomica);

        mSrMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorType = Sensor.TYPE_ORIENTATION;
        setTitle("Poziomica");
    }

    @Override
    protected void onResume() {
        super.onResume();

        final Sensor sens = mSrMgr.getSensorList(mSensorType).get(0);
        mSrMgr.registerListener(this, sens, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();

        mSrMgr.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tv = (TextView) findViewById(R.id.poziomicaValue);
        String value;
        float yOrient = Math.abs(event.values[1]);
        float zOrient = Math.abs(event.values[2]);
        if(yOrient<1 && zOrient<1){
            value = "prosto";
        } else if(yOrient<5 && zOrient<5){
            value = "może być";
        } else {
            value = "krzywo";
        }

        tv.setText(value);
        /*
        sb.append("X rotation: ");
        sb.append(String.format("%7.4f", event.values[0]));
        sb.append(" degrees\nY rotation: ");
        sb.append(String.format("%7.4f", event.values[1]));
        sb.append(" degrees\nZ rotation: ");
        sb.append(String.format("%7.4f", event.values[2]));
        sb.append(" degrees");
        break;
        tv.setText(sb);*/
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
