package com.example.zadanie2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ListRow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_row);

        final Intent intencja = new Intent(this, MyWindow.class);

        Button przycisk12 = (Button) findViewById(R.id.button12);
        przycisk12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja);
            }
        });
    }
}
