package com.example.zadanie6;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Readnote extends AppCompatActivity {

    int liczbaElem;

    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private int bufferSize = 0;

    private class LVitem{

        TextView tv0;
        TextView tv1;
        TextView tv2;
        TextView tv3;

        Button b0;
        Button b1;

        String myPath;
    }

    public class MyAdapter extends BaseAdapter {


        private LayoutInflater inflater= null;
        boolean[] zazn_pozycje;
        LVitem myLVitem;

        MyAdapter(String[] lista){
            super();

            SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);

            zazn_pozycje=new boolean[sp.getInt("num", 0)];
            inflater= (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount(){
            SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            return sp.getInt("num", 0)+1;
        }

        public Object getItem(int position){
            return position;
        }

        public long getItemId(int position){
            return position;
        }

        public View getView(final int position, View listItemView, ViewGroup parent){
            if(listItemView==null){
                listItemView=inflater.inflate(R.layout.activity_list_row, null);
                myLVitem=new LVitem();
                myLVitem.tv0=(TextView) listItemView.findViewById(R.id.textViewR0);
                myLVitem.tv1=(TextView) listItemView.findViewById(R.id.textViewR1);
                myLVitem.tv2=(TextView) listItemView.findViewById(R.id.textViewR2);
                myLVitem.tv3=(TextView) listItemView.findViewById(R.id.textViewR3);
                myLVitem.b0=(Button) listItemView.findViewById(R.id.buttonPlay);
                myLVitem.b1=(Button) listItemView.findViewById(R.id.buttonDelete2);
                listItemView.setTag(myLVitem);
            }else {
                myLVitem=(LVitem)listItemView.getTag();
            }


            final SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            liczbaElem=sp.getInt("num", 0);

            String klucz = "imie"+position;
            myLVitem.tv0.setText(sp.getString(klucz, "DEFAULT"));
            klucz = "nazwisko"+position;
            myLVitem.tv1.setText(sp.getString(klucz, "DEFAULT"));
            klucz = "tytul"+position;
            myLVitem.tv2.setText(sp.getString(klucz, "DEFAULT"));
            klucz = "opis"+position;
            myLVitem.tv3.setText(sp.getString(klucz, "DEFAULT"));


            myLVitem.b0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String klucz = "path"+position;
                    myLVitem.myPath = sp.getString(klucz, "");

                    if(!myLVitem.myPath.equals("")) {
                        Uri uri = Uri.fromFile(new File(myLVitem.myPath));
                        MediaPlayer mp = MediaPlayer.create(Readnote.this, uri);

                        //MediaPlayer mp = new MediaPlayer();
/*
                    try {
                        mp.setDataSource(myLVitem.myPath);
                    }catch (Exception ex){}
*/
                        mp.start();
                    }
                }
            });

            myLVitem.b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SharedPreferences.Editor spe = sp.edit();

                    String u = "Notatka usunięta";
                    String e = "";

                    spe.putString("imie"+position, u);
                    spe.putString("nazwisko"+position, e);
                    spe.putString("tytul"+position, e);
                    spe.putString("opis"+position, e);
                    spe.putString("path"+position, e);
                    spe.commit();

                    finish();
                    startActivity(getIntent());
                    /*
                    myLVitem.tv0.setText(u);
                    myLVitem.tv1.setText(e);
                    myLVitem.tv2.setText(e);
                    myLVitem.tv3.setText(e);
                    */
                }
            });

            return listItemView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_readnote);

        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);

        liczbaElem=sp.getInt("num", 0);

        bufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

        MyAdapter adapter = new MyAdapter(new String[0]);
        ListView lista = (ListView) findViewById(R.id.listView);
        lista.setAdapter(adapter);
    }


    private void CombineWaveFile(String file1, String file2) {
        FileInputStream in1 = null, in2 = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = RECORDER_SAMPLERATE;
        int channels = 2;
        long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

        byte[] data = new byte[bufferSize];

        try {
            in1 = new FileInputStream(file1);
            in2 = new FileInputStream(file2);

            out = new FileOutputStream(""); //FileOutputStream(getFilename3());

            totalAudioLen = in1.getChannel().size() + in2.getChannel().size();
            totalDataLen = totalAudioLen + 36;

            WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                    longSampleRate, channels, byteRate);

            while (in1.read(data) != -1) {

                out.write(data);

            }
            while (in2.read(data) != -1) {

                out.write(data);
            }

            out.close();
            in1.close();
            in2.close();

            //Toast.makeText(this, "Done!!", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void WriteWaveFileHeader(FileOutputStream out, long totalAudioLen, long totalDataLen, long longSampleRate, int channels, long byteRate) throws IOException {

        byte[] header = new byte[44];

        header[0] = 'R';
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);
        header[33] = 0;
        header[34] = RECORDER_BPP;
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

        out.write(header, 0, 44);
    }
}
