package com.example.zadanie2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class List extends AppCompatActivity {


    int liczbaElem;

    private class LVitem{
        /*public LVitem(){
            setContentView(R.layout.list_row);
        }*/
        TextView tv1;
        ImageView img;

        Button btt;

    }

    public class MyAdapter extends BaseAdapter {


        private LayoutInflater inflater= null;
        boolean[] zazn_pozycje;
        LVitem myLVitem;

        MyAdapter(String[] lista){
            super();

            SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);

            zazn_pozycje=new boolean[sp.getInt("num", 0)];
            inflater= (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount(){
            SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            return sp.getInt("num", 0)+1;
        }

        public Object getItem(int position){
            return position;
        }

        public long getItemId(int position){
            return position;
        }

        public View getView(final int position, View listItemView, ViewGroup parent){
            if(listItemView==null){
                listItemView=inflater.inflate(R.layout.activity_list_row, null);
                myLVitem=new LVitem();
                myLVitem.tv1=(TextView) listItemView.findViewById(R.id.textView4);
                myLVitem.img=(ImageView) listItemView.findViewById(R.id.row_image);
                myLVitem.btt= (Button) listItemView.findViewById(R.id.button12);

                listItemView.setTag(myLVitem);
            }else {
                myLVitem=(LVitem)listItemView.getTag();
            }

            final SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            liczbaElem=sp.getInt("num", 0);

            String klucz= "album"+position;
            String klucz2= "gatunek"+position;
            myLVitem.tv1.setText(sp.getString(klucz, "DEFAULT"));
            final int obrazek;
            if (sp.getInt(klucz2, 0) == 0) {
                obrazek=R.drawable.rock;
            }else if(sp.getInt(klucz2, 0) == 1) {
                obrazek=R.drawable.metal;
            }else{
                obrazek=R.drawable.jazz;
            }
            myLVitem.img.setImageResource(obrazek);


            myLVitem.btt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pleaseMakeThisWork(position);
                }
            });

            return listItemView;
        }
    }


    void pleaseMakeThisWork(int n){
        Intent intencja = new Intent(this, MyWindow.class);
        Bundle dane= new Bundle();
        dane.putInt("numer", n);
        intencja.putExtras(dane);
        startActivityForResult(intencja,2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        final LinearLayout ll= findViewById(R.id.listLayout);
        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
        int BGC = sp.getInt("kolor", 255);
        ll.setBackgroundColor(Color.rgb(BGC, BGC, BGC));

        liczbaElem=sp.getInt("num", 0);
        Button przycisk9 = (Button) findViewById(R.id.button9);
        Button przycisk10 = (Button) findViewById(R.id.button10);

        MyAdapter adapter = new MyAdapter(new String[0]);
        ListView lista = (ListView) findViewById(R.id.listView);
        lista.setAdapter(adapter);

        final Intent intencja1 = new Intent(this, Add.class);

        przycisk9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja1);
            }
        });
        przycisk10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

    @Override
    protected void onActivityResult(int reqID, int resC, Intent ii){
        if(resC== Activity.RESULT_OK && reqID==2){

        }
    }
}
