package com.example.zadanie2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MyWindow extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mywindow);

        Intent iii= getIntent();
        Bundle dane= new Bundle();
        dane= iii.getExtras();
        int nr= dane.getInt("numer");

        TextView tekst1=(TextView) findViewById(R.id.windowtxt1);
        TextView tekst2=(TextView) findViewById(R.id.windowtxt2);
        TextView tekst3=(TextView) findViewById(R.id.windowtxt3);
        TextView tekst4=(TextView) findViewById(R.id.windowtxt4);

        ImageView img=(ImageView) findViewById(R.id.image);

        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);

        String klucz= "gatunek"+nr;
        int obrazek;
        if (sp.getInt(klucz, 0) == 0) {
            obrazek=R.drawable.rock;
        }else if(sp.getInt(klucz, 0) == 1) {
            obrazek=R.drawable.metal;
        }else{
            obrazek=R.drawable.jazz;
        }
        img.setImageResource(obrazek);

        String klucz2= "album"+nr;
        tekst1.setText(sp.getString(klucz2, "DEFAULT"));
        String klucz3= "wykonawca"+nr;
        tekst2.setText(sp.getString(klucz3, "DEFAULT"));
        String klucz4= "rok"+nr;
        tekst3.setText("Rok "+sp.getInt(klucz4, 0));
        String klucz5= "rating"+nr;
        tekst4.setText("Ocena "+sp.getFloat(klucz5, 0));

        Button przycisk11 = (Button) findViewById(R.id.button11);

        iii.putExtras(dane);
        setResult(Activity.RESULT_OK, iii);

        przycisk11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }
}
