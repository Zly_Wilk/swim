package com.example.cw2;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Lista3 extends AppCompatActivity {

    String[] lista;
    String[] ltxt1={"Pozycja nr 1", "Pozycja nr 2", "Pozycja nr 3", "Pozycja nr 4", "Pozycja nr 5",
            "Pozycja nr 6", "Pozycja nr 7", "Pozycja nr 8", "Pozycja nr 9", "Pozycja nr 10"};
    String[] ltxt2={"Tekst 1", "Tekst 2", "Tekst 3", "Tekst 4", "Tekst 5", "Tekst 6", "Tekst 7",
            "Tekst 8", "Tekst 9", "Tekst 10"};

    private class LVitem{
        /*public LVitem(){
            setContentView(R.layout.list_row);
        }*/
        TextView tv1;
        TextView tv2;
        ImageView img;
        CheckBox cBox;
    }

    public class MyAdapter2 extends BaseAdapter {

        private LayoutInflater inflater= null;
        boolean[] zazn_pozycje;
        LVitem myLVitem;

        MyAdapter2(String[] lista){
            super();

            zazn_pozycje=new boolean[ltxt1.length];
            inflater= (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount(){
            return ltxt1.length;
        }

        public Object getItem(int position){
            return position;
        }

        public long getItemId(int position){
            return position;
        }

        public View getView(final int position, View listItemView, ViewGroup parent){
            if(listItemView==null){
                listItemView=inflater.inflate(R.layout.list_row, null);
                myLVitem=new LVitem();
                myLVitem.tv1=(TextView) listItemView.findViewById(R.id.row_tv1);
                myLVitem.tv2=(TextView) listItemView.findViewById(R.id.row_tv2);
                myLVitem.img=(ImageView) listItemView.findViewById(R.id.row_image);
                myLVitem.cBox=(CheckBox) listItemView.findViewById(R.id.lrow_checkBox);
                listItemView.setTag(myLVitem);
            }else {
                myLVitem=(LVitem)listItemView.getTag();
            }

            myLVitem.tv1.setText(ltxt1[position]);
            myLVitem.tv2.setText(ltxt2[position]);
            myLVitem.cBox.setChecked(zazn_pozycje[position]);

            myLVitem.cBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked())
                        zazn_pozycje[position]=true;
                    else
                        zazn_pozycje[position]=false;

                    Toast.makeText(getApplicationContext(), "Zaznaczyłeś/odznaczyłeś: "+(position+1),
                            Toast.LENGTH_SHORT).show();


                }
            });

            return listItemView;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista3);

        lista= new String[12];
        for(int i=0; i<lista.length; i++)
            lista[i]= "Element "+i;

        final ListView lista1= (ListView) findViewById(R.id.listView);

        MyAdapter2 adapter = new MyAdapter2(ltxt1);
        ListView lista3 = (ListView) findViewById(R.id.listView);
        lista3.setAdapter(adapter);
        /*ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        lista1.setAdapter(adapter2);

        lista1.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick (AdapterView<?> parent, View view, int position, long id){
                Toast.makeText(getApplicationContext(), lista[position], Toast.LENGTH_SHORT).show();
            }
        });*/


    }
}
