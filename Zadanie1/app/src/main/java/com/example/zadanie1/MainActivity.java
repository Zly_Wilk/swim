package com.example.zadanie1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent intencja1 = new Intent(this, Main2Activity.class);
        final Intent intencja2 = new Intent(this, Main3Activity.class);
        final Intent intencja3 = new Intent(this, Main4Activity.class);

        Button przycisk1 = (Button) findViewById(R.id.button);
        Button przycisk2 = (Button) findViewById(R.id.button2);
        Button przycisk3 = (Button) findViewById(R.id.button3);

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja1);
                Toast.makeText(getApplicationContext(), "Przycisk 1", Toast.LENGTH_SHORT).show();
            }
        });
        przycisk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja2);
                Toast.makeText(getApplicationContext(), "Przycisk 2", Toast.LENGTH_SHORT).show();
            }
        });
        przycisk3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja3);
                Toast.makeText(getApplicationContext(), "Przycisk 3", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
