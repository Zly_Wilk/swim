package com.example.zadanie3;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Activity1 extends AppCompatActivity {

    TextView headText;
    TextView lyricText;

    private ActionMode actionMode;

    private boolean mBold = false;
    private boolean mItalic = false;

    ConstraintLayout cl;

    int zazn = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        headText= (TextView) findViewById(R.id.textView);
        lyricText= (TextView) findViewById(R.id.textView2);

        Button przycisk0 = (Button) findViewById(R.id.button10);
        Button przycisk1 = (Button) findViewById(R.id.button11);
        Button przycisk2 = (Button) findViewById(R.id.button12);

        cl= findViewById(R.id.layout1);

        registerForContextMenu(przycisk0);
        registerForContextMenu(przycisk2);

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actionMode == null)
                    actionMode = startActionMode(actionModeCallback);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu0, menu);

        //MenuInflater inflater1 = getMenuInflater();
        //inflater1.inflate(R.menu.first_checkable,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item0:
                headText.setTextColor(getResources().getColor(R.color.czarny));
                lyricText.setTextColor(getResources().getColor(R.color.czarny));
                break;
            case R.id.item1:
                headText.setTextColor(getResources().getColor(R.color.czerwony));
                lyricText.setTextColor(getResources().getColor(R.color.czerwony));
                break;
            case R.id.item2:
                headText.setTextColor(getResources().getColor(R.color.niebieski));
                lyricText.setTextColor(getResources().getColor(R.color.niebieski));
                break;
            case R.id.subItem0:
                headText.setTextSize(14);
                lyricText.setTextSize(14);
                break;
            case R.id.subItem1:
                headText.setTextSize(18);
                lyricText.setTextSize(18);
                break;
            case R.id.subItem2:
                headText.setTextSize(24);
                lyricText.setTextSize(24);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        switch (v.getId())
        {
            case(R.id.button10):
            {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu1, menu);

            }
            break;
            case(R.id.button12):
            {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu3, menu);
                menu.setHeaderTitle("Kolor tła");
                MenuItem rzecz0 = (MenuItem) menu.getItem(0);
                MenuItem rzecz1 = (MenuItem) menu.getItem(1);
                MenuItem rzecz2 = (MenuItem) menu.getItem(2);
                /*if(zazn==0) {
                    rzecz0.setChecked(true);
                    rzecz1.setChecked(false);
                    rzecz2.setChecked(false);
                }else if(zazn==1){
                    rzecz0.setChecked(false);
                    rzecz1.setChecked(true);
                    rzecz2.setChecked(false);
                }else if(zazn==2){
                    rzecz0.setChecked(false);
                    rzecz1.setChecked(false);
                    rzecz2.setChecked(true);
                }*/
            }
            break;


        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case(R.id.itemHead0):
            {
                headText.setTextColor(getResources().getColor(R.color.czarny));
                return true;
            }
            case(R.id.itemHead1):
            {
                headText.setTextColor(getResources().getColor(R.color.czerwony));
                return true;
            }
            case(R.id.itemHead2):
            {
                headText.setTextColor(getResources().getColor(R.color.niebieski));
            return true;
            }
            case(R.id.itemText0):
            {
                lyricText.setTextSize(14);
                return true;
            }
            case(R.id.itemText1):
            {
                lyricText.setTextSize(18);
                return true;
            }
            case(R.id.itemText2):
            {
                lyricText.setTextSize(24);
                return true;
            }
            case(R.id.item30):
            {
                if(item.isChecked()==true){
                    //item.setChecked(false);
                }else{
                    zazn=0;
                    cl.setBackgroundColor(getResources().getColor(R.color.bialy));
                    item.setChecked(true);

                }
                return true;
            }
            case(R.id.item31):
            {
                if(item.isChecked()==true){
                    //item.setChecked(false);
                }else{
                    zazn=1;
                    cl.setBackgroundColor(getResources().getColor(R.color.niebieski));
                    item.setChecked(true);

                }
                return true;
            }
            case(R.id.item32):
            {
                if(item.isChecked()==true){
                    //item.setChecked(false);
                }else{
                    item.setChecked(true);
                    /*MenuItem rzecz0 = (MenuItem) findViewById(R.id.item30);
                    MenuItem rzecz1 = (MenuItem) findViewById(R.id.item31);
                    rzecz0.setChecked(false);
                    rzecz1.setChecked(false);*/
                    zazn=2;
                    cl.setBackgroundColor(getResources().getColor(R.color.zielony));
                }
                return true;
            }


            default:
                return super.onContextItemSelected(item);
        }



    }

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            mode.getMenuInflater().inflate(R.menu.menu2, menu);
            mode.setTitle("Action Mode");

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {


            switch ((item.getItemId())) {

                case R.id.itemB: {
                    if (mBold==true) {
                        mBold=false;
                    }else {
                        mBold=true;
                    }
                }
                break;
                case R.id.itemI: {
                    if (mItalic==true) {
                        mItalic=false;
                    }else {
                        mItalic=true;
                    }
                }
                break;

            }
            updateTextView();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
        }
    };

    protected void updateTextView(){
        if(mBold && mItalic){
            headText.setTypeface(null, Typeface.BOLD_ITALIC);
            lyricText.setTypeface(null, Typeface.BOLD_ITALIC);
        }else if(mBold && !mItalic){
            headText.setTypeface(null,Typeface.BOLD);
            lyricText.setTypeface(null,Typeface.BOLD);
        }else if(!mBold && mItalic){
            headText.setTypeface(null,Typeface.ITALIC);
            lyricText.setTypeface(null,Typeface.ITALIC);
        }else {
            headText.setTypeface(null,Typeface.NORMAL);
            lyricText.setTypeface(null,Typeface.NORMAL);
        }
    }
}
