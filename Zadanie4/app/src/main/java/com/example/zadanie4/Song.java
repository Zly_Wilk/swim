package com.example.zadanie4;

public class Song {
    private String title;
    private String composer;
    private int lengthInSeconds;
    private int adoreRating;
    private int genre;

    public Song(String title, String composer, int lengthInSeconds, int genre, int adoreRating)
    {
        this.composer=composer;
        this.lengthInSeconds=lengthInSeconds;
        this.title=title;
        this.adoreRating=adoreRating;
        this.genre =genre;
    }

    public int getLengthInSeconds() {
        return lengthInSeconds;
    }

    public void setLengthInSeconds(int lengthInSeconds) {
        this.lengthInSeconds = lengthInSeconds;
    }

    public String getComposer() {
        return composer;
    }

    public String getTitle() {
        return title;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAdoreRating() {
        return adoreRating;
    }

    public void setAdoreRating(int adoreRating) {
        this.adoreRating = adoreRating;
    }

    public int getGenre() {
        return genre;
    }

    public void setGenre(int genre) {
        this.genre = genre;
    }
}
