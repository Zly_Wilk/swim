package com.example.zadanie3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button przycisk0 = findViewById(R.id.button00);
        Button przycisk1 = findViewById(R.id.button01);

        final Intent intencja1 = new Intent(this, Activity1.class);
        final Intent intencja2 = new Intent(this, Activity2.class);

        przycisk0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja1);

            }
        });

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja2);

            }
        });
    }
}
