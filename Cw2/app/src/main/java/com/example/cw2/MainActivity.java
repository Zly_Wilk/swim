package com.example.cw2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String[] lista= {"Pozycja 1", "Pozycja 2", "Pozycja 3"};
    String[] p={"1","2","3"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent intencja1 = new Intent(this, Lista1.class);
        final Intent intencja2 = new Intent(this, Lista2.class);
        final Intent intencja3 = new Intent(this, Grid1.class);
        final Intent intencja4 = new Intent(this, Lista3.class);

        Button przycisk1 = (Button) findViewById(R.id.button);
        Button przycisk2 = (Button) findViewById(R.id.button2);
        Button przycisk3 = (Button) findViewById(R.id.button3);
        Button przycisk4 = (Button) findViewById(R.id.button4);

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja1);
            }
        });
        przycisk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja2);
           }
        });
        przycisk3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja3);
            }
        });
        przycisk4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja4);
            }
        });

        Spinner opcje= (Spinner) findViewById(R.id.spinner1);
        if (opcje!=null){
            opcje.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getApplicationContext(), "Wybrałeś: "+p[position], Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            ArrayAdapter<String> adaper= new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, lista);
            adaper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            opcje.setAdapter(adaper);
        }

        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);
        SharedPreferences.Editor spe=sp.edit();
        int nru = sp.getInt("nr_uruchomienia", 0);
        nru++;
        Toast.makeText(getApplicationContext(), "Uruchomienie nr "+nru, Toast.LENGTH_SHORT).show();
        spe.putInt("nr_uruchomienia", nru);
        spe.commit();
    }
}
