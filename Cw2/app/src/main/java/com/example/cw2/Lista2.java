package com.example.cw2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Lista2 extends AppCompatActivity {

    String[] lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista2);

        lista= new String[12];
        for(int i=0; i<lista.length; i++)
            lista[i]= "Element "+i;

        final ListView lista1= (ListView) findViewById(R.id.listView);
        lista1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(getApplicationContext(), view.toString(), Toast.LENGTH_SHORT).show();

                String napis= "";
                SparseBooleanArray zaznaczone= lista1.getCheckedItemPositions();
                for (int i=0; i<zaznaczone.size(); i++){
                    if(zaznaczone.valueAt(i)){
                        int indeks=zaznaczone.keyAt(i);
                        napis+=(" "+String.valueOf(indeks++));

                    }
                }

                Toast.makeText(getApplicationContext(), "Wybrałeś:"+napis, Toast.LENGTH_SHORT).show();

            }
        });

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, lista);
        lista1.setAdapter(adapter2);


    }


}
