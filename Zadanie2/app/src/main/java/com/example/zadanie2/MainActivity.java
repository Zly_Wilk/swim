package com.example.zadanie2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int BGC;
    String tekstNaglowka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        SharedPreferences sp= getSharedPreferences("dane_plikacji", MODE_PRIVATE);



        final LinearLayout ll= findViewById(R.id.mainLayout);
        BGC = sp.getInt("kolor", 255);
        ll.setBackgroundColor(Color.rgb(BGC, BGC, BGC));

        final TextView tekst = (TextView) findViewById(R.id.tekstNag);
        tekstNaglowka=sp.getString("tekstN", "Zadanie 2");
        tekst.setText(tekstNaglowka);

        String tekstPowitalny = sp.getString("tekstP", "Witam");
        Toast.makeText(getApplicationContext(), tekstPowitalny, Toast.LENGTH_SHORT).show();

        final Intent intencja1 = new Intent(this, Settings.class);
        final Intent intencja2 = new Intent(this, Add.class);
        final Intent intencja3 = new Intent(this, List.class);

        Button przycisk1 = (Button) findViewById(R.id.button);
        Button przycisk2 = (Button) findViewById(R.id.button2);
        Button przycisk3 = (Button) findViewById(R.id.button3);

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(intencja1, 8);

            }
        });
        przycisk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja2);

            }
        });
        przycisk3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja3);

            }
        });

    }

    @Override
    protected void onActivityResult(int reqID, int resC, Intent ii){
        if(resC== Activity.RESULT_OK && reqID==8){
            Bundle dane = new Bundle();
            dane = ii.getExtras();

            BGC= dane.getInt("kolorTla");
            final LinearLayout ll= findViewById(R.id.mainLayout);
            ll.setBackgroundColor(Color.rgb(BGC, BGC, BGC));

            tekstNaglowka= dane.getString("naglowek");
            final TextView tekst = (TextView) findViewById(R.id.tekstNag);
            tekst.setText(tekstNaglowka);
        }
    }
}
