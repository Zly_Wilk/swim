package com.example.zadanie5;

import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

//  https://developer.android.com/guide/topics/sensors/sensors_overview

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public final void startAktywnosci(final View v){

        Intent in = new Intent(this, SensorWindow.class);

        switch (v.getId()){
            case R.id.button0:
                in.putExtra("sensorType", Sensor.TYPE_LIGHT);
                break;
            case R.id.button1:
                in.putExtra("sensorType", Sensor.TYPE_LINEAR_ACCELERATION);
                break;
            case R.id.button2:
                in.putExtra("sensorType", Sensor.TYPE_GRAVITY);
                break;
            case R.id.button3:
                in.putExtra("sensorType", Sensor.TYPE_PROXIMITY);
                break;
            case R.id.button4:
                in.putExtra("sensorType", Sensor.TYPE_GYROSCOPE);
                break;
            case R.id.button5:
                in.putExtra("sensorType", Sensor.TYPE_ORIENTATION);
                break;
            case R.id.button6:
                in = new Intent(this, Poziomica.class);
                break;
            case R.id.button7:
                in = new Intent(this, Dice.class);
                break;
        }
        startActivity(in);
    }
}
