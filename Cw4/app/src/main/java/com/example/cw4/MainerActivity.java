package com.example.cw4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainer);

        Button przycisk0 = findViewById(R.id.button00);
        Button przycisk1 = findViewById(R.id.button01);

        final Intent intencja1 = new Intent(this, MainActivity.class);
        final Intent intencja2 = new Intent(this, Aktownosc2.class);

        przycisk0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja1);

            }
        });

        przycisk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja2);

            }
        });
    }
}
