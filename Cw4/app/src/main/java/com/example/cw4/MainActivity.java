package com.example.cw4;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements Fragment1.OnWyborOpcjiListener{

    Fragment11 f11;
    Fragment12 f12;
    FragmentTransaction transakcja;

    private static final String TAG_F11 = "Fragment11";
    private static final String TAG_F12 = "Fragment12";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button przycisk0 = findViewById(R.id.button);

        final Intent intencja1 = new Intent(this, Aktownosc2.class);

        przycisk0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intencja1);

            }
        });

        if(savedInstanceState==null) {
            f11 = new Fragment11();
            f12 = new Fragment12();
            transakcja = getSupportFragmentManager().beginTransaction();
            transakcja.add(R.id.kontener, f11, TAG_F11);
            transakcja.detach(f11);
            transakcja.add(R.id.kontener, f12, TAG_F12);
            transakcja.detach(f12);
            transakcja.commit();
        } else {
            f11 = (Fragment11) getSupportFragmentManager().findFragmentByTag(TAG_F11);
            f12 = (Fragment12) getSupportFragmentManager().findFragmentByTag(TAG_F12);
        }
    }

    @Override
    public void onWyborOpcji(int opcja) {
        FragmentTransaction transakcja = getSupportFragmentManager().beginTransaction();
        switch(opcja){
            case 1: {
                transakcja.detach(f12);
                transakcja.attach(f11);
            }
                break;
            case 2: {
                transakcja.detach(f11);
                transakcja.attach(f12);
            }
                break;
        }
        transakcja.commit();

    }
}
