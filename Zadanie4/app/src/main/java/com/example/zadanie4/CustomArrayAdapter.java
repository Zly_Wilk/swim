package com.example.zadanie4;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomArrayAdapter extends ArrayAdapter {
    Context context;
    ArrayList<Composer> listOfComposers;
    LayoutInflater layoutInflater;
    Activity activity;

    Boolean isFirst;

    AppPreferences _appPrefs;

    String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};

    public CustomArrayAdapter(Context context, ArrayList<Composer> listOfComposers) {
        super(context,R.layout.list_row, listOfComposers);
        this.context = context;
        this.activity =activity;
        this.listOfComposers = listOfComposers;
        layoutInflater = LayoutInflater.from(context);
        isFirst=true;
        _appPrefs = new AppPreferences(context);

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Composer composer;
        View v = convertView;
        ArrayList<Composer> test = new ArrayList<>();
        if(convertView==null)
        {
            if(_appPrefs.getComposerArray().isEmpty())
            {
                //listOfComposers.add(new Composer("s","d","k",1,2));
                return layoutInflater.inflate(R.layout.empty_composer, parent, false);
            }
            else
                v =  layoutInflater.inflate(R.layout.list_row, parent,false);
        }



        TextView nickname= v.findViewById(R.id.row_tv1);
        //TextView  musicGenre= v.findViewById(R.id.row_tv2);
        TextView  composerGrade= v.findViewById(R.id.textGrade);


        composer= listOfComposers.get(position);
        nickname.setText(composer.getName());
        // musicGenre.setText(musicalGenres[composer.getGenre()]);
        composerGrade.setText(musicalGenres[composer.getGenre()]);



        return v;
    }
}