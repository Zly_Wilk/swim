package com.example.zadanie4;




import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;




public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
     ComposersListFragment composersListFragment;
    private AddingFragment addingFragment;
    private SongsListFragment songsListFragment;

    static  boolean two = false;
    static  boolean thre = false;

    @Override
    public Fragment getItem(int position) {
        if (position == MainActivity._ADDING) {


                addingFragment = AddingFragment.newInstance();

            return addingFragment;
        }
        else if (position == MainActivity._COMPOSERS_LIST)
        {
            if(!two) {
                composersListFragment = ComposersListFragment.newInstance();
                two = true;
            }



            return composersListFragment;
        }
        else if (position == MainActivity._SONGS_LIST)
        {
            if(!thre) {
                songsListFragment = SongsListFragment.newInstance();
                thre = true;
            }
            return songsListFragment;
        }
        else return null;
    }


    @Override
    public int getCount() {
        return 3;
    }
    public int getItemPosition(Object object) {

        return POSITION_NONE;
    }


}