package com.example.cw2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;


public class Lista1 extends AppCompatActivity {

    String[] lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista1);

        lista= new String[12];
        for(int i=0; i<lista.length; i++)
            lista[i]= "Element "+i;

        final ListView lista1= (ListView) findViewById(R.id.listView);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        lista1.setAdapter(adapter2);

        lista1.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick (AdapterView<?> parent, View view, int position, long id){
                Toast.makeText(getApplicationContext(), lista[position], Toast.LENGTH_SHORT).show();
            }
        });


    }


}
