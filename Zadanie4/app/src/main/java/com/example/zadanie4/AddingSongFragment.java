package com.example.zadanie4;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;



import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddingSongFragment extends Fragment {

    String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};

    public AddingSongFragment() {
        // Required empty public constructor
    }
    SeekBar adoreSeekBar;
    AppPreferences _appPrefs;
    int adoreValue=0;
    ArrayList<String> composerList;
    EditText editTitle;
    EditText editTime;

    Spinner composerSpinner;

    SharedPreferences sp;

    String tytul = "";
    int dlugosc = 0;
    int gatunek = 0;
/*
    SeekBar.OnSeekBarChangeListener adoreListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            adoreValue = adoreSeekBar.getProgress();
        }
    };
*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        sp= context.getSharedPreferences("dane_plikacji", MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_adding_song, container, false);
        _appPrefs = new AppPreferences(v.getContext());

        editTitle = v.findViewById(R.id.editTitle);
        editTime = v.findViewById(R.id.editTime);

        composerSpinner= v.findViewById(R.id.composerSpinner);
        //adoreSeekBar = v.findViewById(R.id.adoreSeekBarSong);
/*
        composerList = new ArrayList<>();

        for(Composer composer : _appPrefs.getComposerArray())
            composerList.add(composer.toString());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, composerList);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        composerSpinner.setAdapter(adapter);
*/
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, musicalGenres);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        composerSpinner.setAdapter(adapter);

        //adoreSeekBar.setOnSeekBarChangeListener(adoreListener);



        return v;
    }

    private Song collectSongData()
    {
        String title = editTitle.getText().toString();
        int length =  !editTime.getText().toString().equals("") ? Integer.parseInt(editTime.getText().toString()) : 0;
        int composerPosition = composerSpinner.getSelectedItemPosition();
        int adore = 0;
        String composerName = "";

        tytul = title;
        dlugosc = length;
        gatunek = composerPosition;

        SharedPreferences.Editor spe = sp.edit();

        int temp = sp.getInt("num1", -1);
        temp++;

        spe.putString("tytul"+temp, tytul);
        spe.putString("dlugosc"+temp, ""+dlugosc);
        spe.putInt("gatunek1"+temp, gatunek);
        spe.putInt("num1", temp);
        spe.commit();

        return new Song(title,composerName,length, composerPosition, adore);
    }
    public void addSong(View view)
    {

        Song newSong = collectSongData();
        if(!checkCollectedData(newSong))
        {

            return;
        }
        ArrayList<Song> newSongArray;
        newSongArray = _appPrefs.getSongArray();
        newSongArray.add(newSong);

        _appPrefs.saveSongArray(newSongArray);

    }
    public void resetElementsSong(View view)
    {
        editTitle.setText("");
        editTime.setText("");
        composerSpinner.setSelection(0);

        tytul = "";
        dlugosc = 0;
        gatunek = 0;
    }
    private boolean checkCollectedData(Song song)
    {
        if(song.getTitle().equals("")) return false;

        if(song.getLengthInSeconds()==0) return false;
        if(song.getGenre()==0) return false;

        return true;



    }
}
