package com.example.zadanie4;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;



import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */


public class AddingCompFragment extends Fragment {

    String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};
    SeekBar adoreSeekBar;
    AppPreferences _appPrefs;
    int adoreValue= 0;

    EditText textName;
    EditText textSurname;
    EditText textTitle;
    Spinner genreSpinner;

    String album="";
    String wykonawca="";
    int rok=-1;
    float rating=-1;
    int gatunek=-1;

    SharedPreferences sp;
    /*
    SeekBar.OnSeekBarChangeListener adoreListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            adoreValue = adoreSeekBar.getProgress();
        }
    };*/

    public AddingCompFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        sp= context.getSharedPreferences("dane_plikacji", MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_adding_comp, container, false);
        _appPrefs = new AppPreferences(v.getContext());

        textName = v.findViewById(R.id.editTextName);
        textSurname = v.findViewById(R.id.editTextSurname);
        textTitle = v.findViewById(R.id.editTextTitle);
        genreSpinner= v.findViewById(R.id.genreSpinner);
        //adoreSeekBar = v.findViewById(R.id.seekBar);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, musicalGenres);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        genreSpinner.setAdapter(adapter);







        return v;

    }



    private boolean checkCollectedData(Composer composer)
    {
        if(composer.getName().equals("")) return false;
        if(composer.getSurname().equals("")) return false;
        if(composer.getNickname()==0) return false;
        if(composer.getGenre()==0) return false;

        return true;



    }
    public void addComposer(View view)
    {

        Composer newComposer = collectComposerData();
        if(!checkCollectedData(newComposer))
        {
            return;
        }
        ArrayList<Composer> newComposerArray;
        newComposerArray = _appPrefs.getComposerArray();
        newComposerArray.add(newComposer);

        _appPrefs.saveComposerArray(newComposerArray);



    }
    private Composer collectComposerData()
    {
        String name = textName.getText().toString();
        String surname = textSurname.getText().toString();
        int title = !textTitle.getText().toString().equals("") ? Integer.parseInt(textTitle.getText().toString()) : 0;
        int genre = genreSpinner.getSelectedItemPosition();
        int adore = 0;

        album = name;
        wykonawca = surname;
        rok = title;
        gatunek = genre;

        SharedPreferences.Editor spe = sp.edit();



        int temp = sp.getInt("num0", -1);
        temp++;

        spe.putString("album"+temp, album);
        spe.putString("wykonawca"+temp, wykonawca);
        spe.putString("rok"+temp, ""+rok);
        spe.putInt("gatunek0"+temp, gatunek);
        spe.putInt("num0", temp);
        spe.commit();

        return new Composer(name,surname,title,genre,adore);
    }
    public void resetElements(View view)
    {
        textName.setText("");
        textSurname.setText("");
        textTitle.setText("");
        genreSpinner.setSelection(0);

        album = "";
        wykonawca = "";
        rok = -1;
        gatunek = -1;
    }



}
