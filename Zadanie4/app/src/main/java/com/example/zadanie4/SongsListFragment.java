package com.example.zadanie4;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;



import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SongsListFragment extends Fragment {

    String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};
    //public CustomArrayAdapter adapter;
    public MyAdapter adapter;
    public ArrayList<Song> songArray;
    ListView songListView;
    AppPreferences _appPrefs;
    Context thisContext;

    public static SongsListFragment newInstance() {
        return new SongsListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        thisContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_songs_list, container, false);

        _appPrefs = new AppPreferences(v.getContext());
        songListView = v.findViewById(R.id.songListView);
        songArray = new ArrayList<>();
        songArray = _appPrefs.getSongArray();


        //adapter = new CustomArrayAdapter(v.getContext(), songArray);
        adapter = new MyAdapter(new String[0]);
        songListView.setAdapter(adapter);
        songListView.setOnItemClickListener( new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    //ArrayList<Song> songArray = _appPrefs.getSongArray();
                    //ArrayList<Composer> composerArray = _appPrefs.getComposerArray();
                    //Composer composer = composerArray.get(position);
                    Song song = songArray.get(position);
                    Intent intent = new Intent(view.getContext(), SongInfo.class);


                    intent.putExtra("textInfoName", " " + song.getTitle());
                    intent.putExtra("textInfoTime", " " + song.getLengthInSeconds());
                    intent.putExtra("textInfoGenre", song.getGenre());
                    intent.putExtra("textInfoAdore", " ");
                    startActivity(intent);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

        });


        return v;
    }
    public void listRefresh() {
        songArray = _appPrefs.getSongArray();
        //adapter = new CustomArrayAdapter(getContext(), songArray);
        adapter = new MyAdapter(new String[0]);
        songListView.setAdapter(adapter);

    }

    public class CustomArrayAdapter extends ArrayAdapter {
        Context context;
        ArrayList<Song> listOfComposers;
        LayoutInflater layoutInflater;
        Activity activity;

        Boolean isFirst;

        AppPreferences _appPrefs;

        String[] musicalGenres = {"Gatunek", "Rock", "Metal", "Jazz", "Blues", "Symfoniczna"};

        public CustomArrayAdapter(Context context, ArrayList<Song> listOfComposers) {
            super(context,R.layout.list_row, listOfComposers);
            this.context = context;
            this.activity =activity;
            this.listOfComposers = listOfComposers;
            layoutInflater = LayoutInflater.from(context);
            isFirst=true;
            _appPrefs = new AppPreferences(context);

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Song song;
            View v = convertView;
            if(convertView==null)
            {
                if(_appPrefs.getSongArray().isEmpty())
                    return layoutInflater.inflate(R.layout.empty_composer, parent, false);
                else
                    v =  layoutInflater.inflate(R.layout.list_row_song, parent,false);
            }



            TextView title= v.findViewById(R.id.row_tv1);
            //TextView  musicGenre= v.findViewById(R.id.row_tv2);
            TextView  grade= v.findViewById(R.id.textGrade);

            song= listOfComposers.get(position);

            title.setText(song.getTitle());

            grade.setText(musicalGenres[song.getGenre()]);




            return v;
        }
    }

    int liczbaElem;

    private class LVitem{

        TextView tv1;
        TextView tv2;

        Button btt;

    }

    public class MyAdapter extends BaseAdapter {


        private LayoutInflater inflater= null;
        boolean[] zazn_pozycje;
        SongsListFragment.LVitem myLVitem;

        MyAdapter(String[] lista){
            super();

            SharedPreferences sp= thisContext.getSharedPreferences("dane_plikacji", MODE_PRIVATE);

            zazn_pozycje=new boolean[sp.getInt("num1", 0)];
            inflater= (LayoutInflater) thisContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount(){
            SharedPreferences sp= thisContext.getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            return sp.getInt("num1", 0)+1;
        }

        public Object getItem(int position){
            return position;
        }

        public long getItemId(int position){
            return position;
        }

        public View getView(final int position, View listItemView, ViewGroup parent){
            if(listItemView==null){
                listItemView=inflater.inflate(R.layout.list_row, null);
                myLVitem=new SongsListFragment.LVitem();
                myLVitem.tv1=(TextView) listItemView.findViewById(R.id.row_tv1);
                myLVitem.tv2=(TextView) listItemView.findViewById(R.id.textGrade);
                myLVitem.btt= (Button) listItemView.findViewById(R.id.button2);

                listItemView.setTag(myLVitem);
            }else {
                myLVitem=(SongsListFragment.LVitem)listItemView.getTag();
            }

            final SharedPreferences sp= thisContext.getSharedPreferences("dane_plikacji", MODE_PRIVATE);
            liczbaElem=sp.getInt("num1", 0);

            String klucz= "tytul"+position;
            String klucz2= "gatunek1"+position;
            myLVitem.tv1.setText(sp.getString(klucz, "DEFAULT"));
            myLVitem.tv2.setText(musicalGenres[sp.getInt(klucz2, 0)]);


            myLVitem.btt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pleaseMakeThisWork(position);
                }
            });


            return listItemView;
        }
    }


    void pleaseMakeThisWork(int n){
        Intent intencja = new Intent(thisContext, SongInfo.class);
        Bundle dane= new Bundle();
        dane.putInt("numer", n);
        intencja.putExtras(dane);
        startActivityForResult(intencja,2);
    }

    /*
    @Override
    protected void onActivityResult(int reqID, int resC, Intent ii){
        if(resC== Activity.RESULT_OK && reqID==2){

        }
    }*/
}
